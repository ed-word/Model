import pandas
import numpy as np
from sklearn import preprocessing, cross_validation, svm

from sklearn.metrics import recall_score
import pickle


# load data
df = pandas.read_csv('FI_Merge.csv', low_memory=False)
Y = np.array(df["FI_FDiagnosis"], dtype=np.uint8)
df = df.drop("FI_FDiagnosis", axis=1)
X = np.array(df)


X_train, X_test, y_train, y_test = cross_validation.train_test_split(X,Y,test_size=0.2)


Classifier = svm.SVC(decision_function_shape='ovo', kernel='rbf', class_weight='balanced')
Classifier.fit(X_train, y_train)

#Save Model
with open("svm_picklefile.p", 'wb') as pickle_file:
    pickle.dump(Classifier, pickle_file)